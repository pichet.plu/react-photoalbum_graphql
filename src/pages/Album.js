import React, { Component } from 'react'
import { gql, graphql } from 'react-apollo'
import AlbumList from '../components/Albums/AlbumList'

class Album extends Component {
    render() {
        const { data: {albumsByUser, error, loading} } = this.props
        if (error) {
            return <div>Error....Not Load Album</div>
        }

        return (
            <div>
                <h1>Album {this.props.params.title}</h1>
                {loading && <div>Loading...</div>}
                {albumsByUser && <AlbumList data={albumsByUser} />}
            </div>
        )
    }
}

const query = gql`
query album($userId: Int){                
    albumsByUser(userId: $userId){
        userId
        id
        title
    }
  }
`
const GraphQL = graphql(query, {
    options(props) {
        return {
            variables: {
                userId: props.params.userID
            }
        }
    }
}
)(Album)
export default GraphQL