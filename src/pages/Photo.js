import React, { Component } from 'react'
import { graphql, gql } from 'react-apollo'
import PhotoList from '../components/Photos/PhotoList'

class Photo extends Component {

    render() {
        const { data: {photosByAlbum, error, loading} } = this.props
        if (error) {
            return <div>Error....Not Load Photo</div>
        }

        return (
            <div>
                <h1>Photos by {this.props.params.title}</h1>
                {loading && <div>Loading...</div>}
                {photosByAlbum && <PhotoList data={photosByAlbum} />}
            </div>
        )
    }
}

const query = gql`
query photo($albumId: Int){                
    photosByAlbum(albumId: $albumId){
        albumId
        id
        title
        thumbnailUrl
    }
  }
`
const GraphQL = graphql(query, {
    options(props) {
        return {
            variables: {
                albumId: props.params.albumID
            }
        }
    }
}
)(Photo)
export default GraphQL