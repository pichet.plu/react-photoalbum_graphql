import React, { Component } from 'react'
import { gql, graphql } from 'react-apollo'
import UserList from '../components/Users/UserList'

class Home extends Component {
    render() {
        const { data: {allUser, error, loading} } = this.props
        if (error) {
            return <div>Error....</div>
        }

        return (
            <div>
                <h1>Users</h1>
                {loading && <div>Loading...</div>}
                {allUser && <UserList data={allUser} />}
            </div>
        )
    }
}

const GraphQL = graphql(gql`
query {                
    allUser{
      id
      name
      username
      email
    }
  }
`
)(Home)
export default GraphQL