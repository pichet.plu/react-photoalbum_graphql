import React from 'react'
import ReactDOM from 'react-dom'
import './index.css'
import registerServiceWorker from './registerServiceWorker'

import { Router, browserHistory } from 'react-router'
import routes from './routes'

import { createStore } from 'redux'
import reducers from './reducers'

import { ApolloClient, ApolloProvider, createNetworkInterface } from 'react-apollo'

const networkInterface = createNetworkInterface({
    uri: 'http://localhost:3009/api/graphql'
})

const client = new ApolloClient({
    networkInterface
})

const store = createStore(reducers)

ReactDOM.render(
    <ApolloProvider client={client} store={store}>
        <Router
            history={browserHistory}
            routes={routes}
        />
    </ApolloProvider>,
    document.getElementById('root')
);
registerServiceWorker();
